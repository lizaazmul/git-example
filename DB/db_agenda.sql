-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2019 at 06:00 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_agenda`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pejabat`
--

CREATE TABLE `tbl_pejabat` (
  `id_pejabat` int(11) NOT NULL,
  `id_skpd` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `unitkerja` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pimpinan`
--

CREATE TABLE `tbl_pimpinan` (
  `id_pimpinan` int(10) NOT NULL,
  `hari` varchar(10) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `pejabat` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `jammulai` varchar(255) NOT NULL,
  `jamselesai` varchar(255) NOT NULL,
  `kegiatan` varchar(255) NOT NULL,
  `dasarkegiatan` varchar(255) NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pimpinan`
--

INSERT INTO `tbl_pimpinan` (`id_pimpinan`, `hari`, `tgl`, `pejabat`, `jabatan`, `jammulai`, `jamselesai`, `kegiatan`, `dasarkegiatan`, `lokasi`, `keterangan`, `file`) VALUES
(3, 'SENIN', '2019-04-08', 'Ir.H.M.Hatta Rahman, Mm', 'a', '09:00', '11:00', 'a', 'a', 'Baruga C', 'Tidak Ada', 'c&eacute;u+azul.jpg'),
(4, 'RABU', '2019-04-10', 'A.Davied Syamsuddin, S.STP, M.Si', 'Kepala Dinas Komunikasi Dan Informatika', '08:00', '00:00', 'penyuluhan', 'penyuluhan', 'kec.turikale', 'Ada', 'Image (3).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_skpd`
--

CREATE TABLE `tbl_skpd` (
  `id_skpd` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(10) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` enum('admin','superadmin','user') NOT NULL,
  `posisi` enum('bupati','wabup','skpd','') NOT NULL,
  `id_skpd` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `username`, `password`, `level`, `posisi`, `id_skpd`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'bupati', 0),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user', 'bupati', 0),
(6, 'liza', 'c4ba6610f5f3b1227a570a14211e7961', 'user', 'bupati', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pejabat`
--
ALTER TABLE `tbl_pejabat`
  ADD PRIMARY KEY (`id_pejabat`);

--
-- Indexes for table `tbl_pimpinan`
--
ALTER TABLE `tbl_pimpinan`
  ADD PRIMARY KEY (`id_pimpinan`);

--
-- Indexes for table `tbl_skpd`
--
ALTER TABLE `tbl_skpd`
  ADD PRIMARY KEY (`id_skpd`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pejabat`
--
ALTER TABLE `tbl_pejabat`
  MODIFY `id_pejabat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pimpinan`
--
ALTER TABLE `tbl_pimpinan`
  MODIFY `id_pimpinan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_skpd`
--
ALTER TABLE `tbl_skpd`
  MODIFY `id_skpd` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
