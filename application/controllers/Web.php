<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	public function index()
	{
		$ceks = $this->session->userdata('agenda@2017');
		$level = $this->session->userdata('level@2017');
		if(!isset($ceks)) {
			redirect('web/login');
		}else{
			$data['user']  			  = $this->Mcrud->get_data_by_pk('tbl_user', 'username', $ceks);

			$this->load->view('header', $data);
			if ($level == "admin") {
					$data['pimpinan']  	= $this->Mcrud->get_data('tbl_pimpinan');
					$this->load->view('beranda', $data);
			}else{
				$data['v_pimpinan']   = $this->Mcrud->get_data('tbl_pimpinan');
				$this->load->view('agenda/pimpinan', $data);
			}
			$this->load->view('footer');
		}
	}


	public function login()
	{
		$ceks = $this->session->userdata('agenda@2017');
		if(isset($ceks)) {
			//$this->load->view('404_content');
			redirect('');
		}else{
			$this->load->view('web/header');
			$this->load->view('web/login');
			$this->load->view('web/footer');

				if (isset($_POST['btnlogin'])){
						 $username = htmlentities(strip_tags($_POST['username']));
						 $pass	   = htmlentities(strip_tags(md5($_POST['password'])));

						 $query  = $this->Mcrud->get_data_by_pk('tbl_user', 'username', $username);
						 $cek    = $query->result();
						 $cekun  = $cek[0]->username;
						 $jumlah = $query->num_rows();

						 if($jumlah == 0) {
								 $this->session->set_flashdata('msg',
									 '
									 <div class="alert alert-danger alert-dismissible" role="alert">
									 		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times; &nbsp;</span>
											</button>
											<strong>Username "'.$username.'"</strong> belum terdaftar.
									 </div>'
								 );
								 redirect('web/login');
						 } else {
										 $row = $query->row();
										 $cekpass = $row->password;
										 if($cekpass <> $pass) {
												$this->session->set_flashdata('msg',
													 '<div class="alert alert-warning alert-dismissible" role="alert">
													 		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																<span aria-hidden="true">&times; &nbsp;</span>
															</button>
															<strong>Username atau Password Salah!</strong>.
													 </div>'
												);
												redirect('web/login');
										 } else {

																$this->session->set_userdata('agenda@2017', "$cekun");
																$this->session->set_userdata('level@2017', "$row->level");

																redirect('web');
										 }
						 }
				}
		}
	}


	public function logout() {
     if ($this->session->has_userdata('agenda@2017')) {
         $this->session->sess_destroy();
         redirect('');
     }
     redirect('');
  }


	public function lupa_password()
	{
		$ceks = $this->session->userdata('agenda@2017');
		if(isset($ceks)) {
			$this->load->view('404_content');
		}else{
			$this->load->view('web/header');
			$this->load->view('web/lupa_password');
			$this->load->view('web/footer');
		}
	}

	function error_not_found(){
		$this->load->view('404_content');
	}




	public function profile()
	{
		$ceks = $this->session->userdata('agenda@2017');
		if(!isset($ceks)) {
			redirect('web/login');
		}else{
			$data['user']  			    = $this->Mcrud->get_data_by_pk('tbl_user', 'username', $ceks);

					$this->load->view('header', $data);
					$this->load->view('profile', $data);
					$this->load->view('footer');

					if (isset($_POST['btnupdate'])) {
						$username 	= htmlentities(strip_tags($this->input->post('username')));
						$password 	= htmlentities(strip_tags($this->input->post('password')));

						$cek_data = $this->Mcrud->get_data_by_pk('tbl_user', 'username', $username);
						if ($cek_data->num_rows() == 0) {
								$status = "update";
						}else{
								if ($username == $ceks) {
										$status = "update";
								}else{
										$status = "";
								}
						}

						if ($status == "update") {
								$data = array(
									'username'	=> $username,
									'password'	=> md5($password)
								);
								$this->Mcrud->update_data('tbl_user', array('username' => $ceks), $data);
								$this->session->has_userdata('agenda@2017');
								$this->session->set_userdata('agenda@2017', "$username");
								$this->session->set_flashdata('msg',
									'
									<div class="alert alert-success alert-dismissible" role="alert">
										 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
											 <span aria-hidden="true">&times; &nbsp;</span>
										 </button>
										 <strong>Sukses!</strong> Berhasil disimpan.
									</div>'
								);
						}else{
								$this->session->set_flashdata('msg',
									'
									<div class="alert alert-warning alert-dismissible" role="alert">
										 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
											 <span aria-hidden="true">&times; &nbsp;</span>
										 </button>
										 <strong>Gagal!</strong> Username '.$username.' sudah ada.
									</div>'
								);
						}

						redirect('web/profile');
					}

		}
	}


	public function user()
	{
		$ceks = $this->session->userdata('agenda@2017');
		$level = $this->session->userdata('level@2017');
		if ($level != "admin") {
			redirect('');
		}
		if(!isset($ceks)) {
			redirect('web/login');
		}else{
			$data['user']  			    = $this->Mcrud->get_data_by_pk('tbl_user', 'username', $ceks);
			$data['v_user']   			= $this->Mcrud->get_data_by_pk('tbl_user', 'level', 'user');

					$this->load->view('header', $data);
					$this->load->view('user', $data);
					$this->load->view('footer');

					if (isset($_POST['btnsimpan'])) {
							$username 		= htmlentities(strip_tags($_POST['username']));
							$password 		= htmlentities(strip_tags($_POST['password']));

							$cek_data = $this->Mcrud->get_data_by_pk('tbl_user', 'username', $username);
							if ($cek_data->num_rows() == 0) {
										$data = array(
											'username'			=> $username,
											'password'			=> $password,
											'level'					=> 'user'
											);
										$this->Mcrud->save_data('tbl_user', $data);

										$this->session->set_flashdata('msg',
											 '
											 <div class="alert alert-success alert-dismissible" role="alert">
													<button type="button" class="close" data-dismiss="alert" aria-label="Close">
														<span aria-hidden="true">&times; &nbsp;</span>
													</button>
													<strong>Sukses!</strong> Agenda Kegiatan Umum berhasil ditambahkan.
											 </div>'
										 );
							}else{
									$this->session->set_flashdata('msg',
										 '
										 <div class="alert alert-warning alert-dismissible" role="alert">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">&times; &nbsp;</span>
												</button>
												<strong>Warning!</strong> Username sudah ada.
										 </div>'
									 );
							}

							redirect('web/user');
					}


		}
	}

	public function user_hapus($id='')
	{
		$ceks = $this->session->userdata('agenda@2017');
		$level = $this->session->userdata('level@2017');
		if ($level != "admin") {
			redirect('');
		}
		if(!isset($ceks)) {
			redirect('web/login');
		}else{
					$this->Mcrud->delete_data_by_pk('tbl_user', 'id_user', $id);

					$this->session->set_flashdata('msg',
						 '
						 <div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times; &nbsp;</span>
								</button>
								<strong>Sukses!</strong> User berhasil dihapus.
						 </div>'
					 );
					 redirect('web/user');
		}
	}

	public function pimpinan()
	{
		$ceks = $this->session->userdata('agenda@2017');
		$level = $this->session->userdata('level@2017');
		if ($level != "admin") {
			redirect('');
		}
		if(!isset($ceks)) {
			redirect('web/login');
		}else{
			$data['user']  			    = $this->Mcrud->get_data_by_pk('tbl_user', 'username', $ceks);
			$data['v_pimpinan']			= $this->Mcrud->get_data('tbl_pimpinan');

					$this->load->view('header', $data);
					$this->load->view('agenda/pimpinan', $data);
					$this->load->view('footer');

					if (isset($_POST['btnsimpan'])) {
							$hari 		= htmlentities(strip_tags($_POST['hari']));
							$tgl 			= htmlentities(strip_tags($_POST['tgl']));
							$pejabat = htmlentities(strip_tags($_POST['pejabat']));
							$jabatan = htmlentities(strip_tags($_POST['jabatan']));
							$jammulai 			= htmlentities(strip_tags($_POST['jammulai']));
							$jamselesai 			= htmlentities(strip_tags($_POST['jamselesai']));
							$kegiatan = htmlentities(strip_tags($_POST['kegiatan']));
							$dasarkegiatan = htmlentities(strip_tags($_POST['dasarkegiatan']));
							$lokasi 	= htmlentities(strip_tags($_POST['lokasi']));
							$keterangan 			= htmlentities(strip_tags($_POST['keterangan']));
							$file 			= htmlentities(strip_tags($_POST['file']));

									$data = array(
										'hari'				=> $hari,
										'tgl'				=> $tgl,
										'pejabat'			=> $pejabat,
										'jabatan'			=> $jabatan,
										'jammulai'			=> $jammulai,
										'jamselesai'		=> $jamselesai,
										'kegiatan'			=> $kegiatan,
										'dasarkegiatan'		=> $dasarkegiatan,
										'lokasi'			=> $lokasi,
										'keterangan'		=> $keterangan,
										'file'				=> $file
										);
									$this->Mcrud->save_data('tbl_pimpinan', $data);

									$this->session->set_flashdata('msg',
										 '
										 <div class="alert alert-success alert-dismissible" role="alert">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">&times; &nbsp;</span>
												</button>
												<strong>Sukses!</strong> Agenda Kegiatan Pimpinan berhasil ditambahkan.
										 </div>'
									 );

							redirect('web/pimpinan');
					}


		}
	}

	public function pimpinan_edit($id='')
	{
		$ceks = $this->session->userdata('agenda@2017');
		$level = $this->session->userdata('level@2017');
		if ($level != "admin") {
			redirect('');
		}
		if(!isset($ceks)) {
			redirect('web/login');
		}else{
			$data['user']  			    = $this->Mcrud->get_data_by_pk('tbl_user', 'username', $ceks);
			$data['v_pimpinan']   	= $this->Mcrud->get_data_by_pk('tbl_pimpinan', 'id_pimpinan', $id);

			if ($data['v_pimpinan']->num_rows() == 0){
				redirect('web/pimpinan');
			}else{
				$data['v_pimpinan'] = $data['v_pimpinan']->row();
			}

					$this->load->view('header', $data);
					$this->load->view('agenda/pimpinan_edit', $data);
					$this->load->view('footer');

					if (isset($_POST['btnsimpan'])) {
							$hari 		= htmlentities(strip_tags($_POST['hari']));
							$tgl 			= htmlentities(strip_tags($_POST['tgl']));
							$pejabat = htmlentities(strip_tags($_POST['pejabat']));
							$jabatan = htmlentities(strip_tags($_POST['jabatan']));
							$jammulai 			= htmlentities(strip_tags($_POST['jammulai']));
							$jamselesai 			= htmlentities(strip_tags($_POST['jamselesai']));
							$kegiatan = htmlentities(strip_tags($_POST['kegiatan']));
							$dasarkegiatan = htmlentities(strip_tags($_POST['dasarkegiatan']));
							$lokasi 	= htmlentities(strip_tags($_POST['lokasi']));
							$keterangan 			= htmlentities(strip_tags($_POST['keterangan']));
							$file 			= htmlentities(strip_tags($_POST['file']));

									$data = array(
										'hari'				=> $hari,
										'tgl'				=> $tgl,
										'pejabat'			=> $pejabat,
										'jabatan'			=> $jabatan,
										'jammulai'			=> $jammulai,
										'jamselesai'		=> $jamselesai,
										'kegiatan'			=> $kegiatan,
										'dasarkegiatan'		=> $dasarkegiatan,
										'lokasi'			=> $lokasi,
										'keterangan'		=> $keterangan,
										'file'				=> $file
								);
								$this->Mcrud->update_data('tbl_pimpinan', array('id_pimpinan' => $id), $data);

								$this->session->set_flashdata('msg',
									 '
									 <div class="alert alert-success alert-dismissible" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times; &nbsp;</span>
											</button>
											<strong>Sukses!</strong> Agenda Kegiatan Pimpinan berhasil diubah.
									 </div>'
								 );
								 redirect('web/pimpinan');

					}

		}
	}


	public function pimpinan_detail($id='')
	{
		$ceks = $this->session->userdata('agenda@2017');
		if(!isset($ceks)) {
			redirect('web/login');
		}else{
			$data['user']  			    = $this->Mcrud->get_data_by_pk('tbl_user', 'username', $ceks);
			$data['v_pimpinan']   	= $this->Mcrud->get_data_by_pk('tbl_pimpinan', 'id_pimpinan', $id);

			if ($data['v_pimpinan']->num_rows() == 0){
				redirect('web/pimpinan');
			}else{
				$data['v_pimpinan'] = $data['v_pimpinan']->row();
			}

					$this->load->view('header', $data);
					$this->load->view('agenda/pimpinan_detail', $data);
					$this->load->view('footer');

		}
	}


	public function pimpinan_hapus($id='')
	{
		$ceks = $this->session->userdata('agenda@2017');
		$level = $this->session->userdata('level@2017');
		if ($level != "admin") {
			redirect('');
		}
		if(!isset($ceks)) {
			redirect('web/login');
		}else{
					$this->Mcrud->delete_data_by_pk('tbl_pimpinan', 'id_pimpinan', $id);

					$this->session->set_flashdata('msg',
						 '
						 <div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times; &nbsp;</span>
								</button>
								<strong>Sukses!</strong> Agenda Kegiatan Pimpinan berhasil dihapus.
						 </div>'
					 );
					 redirect('web/pimpinan');

		}
	}
}
