<!-- Main content -->
<div class="content-wrapper">
  <br><br><br>
  <!-- Content area -->
  <div class="content">

    <!-- Dashboard content -->
    <div class="row">
      <!-- Basic datatable -->
      <div class="panel panel-flat">
        <div class="panel-heading">
          <h5 class="panel-title">Detail Agenda Kegiatan Pimpinan</h5>
          <div class="heading-elements">
            <ul class="icons-list">
              <li><a data-action="collapse"></a></li>
            </ul>
          </div>
        </div>
        <hr>
        <div class="panel-body">
          <?php
          echo $this->session->flashdata('msg');
          ?>

          <table width="100%">
            <tr>
              <td width="10%">Hari</td>
              <td width="1%">:</td>
              <td> <?php echo $v_pimpinan->hari; ?></td>
            </tr>
            <tr>
              <td>Tanggal</td>
              <td width="1%">:</td>
              <td> <?php echo $v_pimpinan->tgl; ?></td>
            </tr>
            <tr>
              <td>Pejabat</td>
              <td width="1%">:</td>
              <td> <?php echo $v_pimpinan->pejabat; ?></td>
            </tr>
            <tr>
              <td>Jabatan</td>
              <td width="1%">:</td>
              <td> <?php echo $v_pimpinan->jabatan; ?></td>
            </tr>
            <tr>
              <td>Jam Mulai</td>
              <td width="1%">:</td>
              <td> <?php echo $v_pimpinan->jammulai; ?></td>
            </tr>
            <tr>
              <td>Jam Selesai</td>
              <td width="1%">:</td>
              <td> <?php echo $v_pimpinan->jamselesai; ?></td>
            </tr>
            <tr>
              <td>Kegiatan</td>
              <td width="1%">:</td>
              <td> <?php echo $v_pimpinan->kegiatan; ?></td>
            </tr>
            <tr>
              <td>Dasar Kegiatan</td>
              <td width="1%">:</td>
              <td> <?php echo $v_pimpinan->dasarkegiatan; ?></td>
            </tr>
            <tr>
              <td>Lokasi</td>
              <td width="1%">:</td>
              <td> <?php echo $v_pimpinan->lokasi; ?></td>
            </tr>
            <tr>
              <td>Keterangan</td>
              <td width="1%">:</td>
              <td> <?php echo $v_pimpinan->keterangan; ?></td>
            </tr>
            <tr>
              <td>file</td>
              <td width="1%">:</td>
              <td> <?php echo $v_pimpinan->file; ?></td>
            </tr>
          </table>

            <br>
            <hr>
            <a href="<?php if ($this->session->userdata('level@2019') == "admin") {?> web/pimpinan <?php }?>" class="btn btn-default">Kembali</a>

        </div>
        <br>

      </div>
      <!-- /basic datatable -->
    </div>
    <!-- /dashboard content -->
