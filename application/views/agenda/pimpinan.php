<?php
if ($this->session->userdata('level@2017') == "admin") {?>
<!-- Main content -->
<div class="content-wrapper">
  <br><br><br>
  <!-- Content area -->
  <div class="content">
<?php
} ?>
    <!-- Dashboard content -->
    <div class="row">
      <!-- Basic datatable -->
      <div class="panel panel-flat">
        <div class="panel-heading">
          <h5 class="panel-title">
            <?php
            if ($this->session->userdata('level@2017') == "admin") {?>  Tambah <?php } ?> Agenda Kegiatan Pimpinan</h5>
          <div class="heading-elements">
            <ul class="icons-list">
              <li><a data-action="collapse"></a></li>
            </ul>
          </div>
        </div>
        <hr>
        <?php
        if ($this->session->userdata('level@2017') == "admin") {?>

        <div class="panel-body">
          <?php
          echo $this->session->flashdata('msg');
          ?>
          <form class="form-horizontal" action="" method="post">
            <div class="col-md-12">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-lg-2">Hari</label>
                  <div class="col-lg-3">
                    <select class="form-control" name="hari" required>
                      <option value="" selected>Pilih</option>
                      <option value="SENIN">SENIN</option>
                      <option value="SELASA">SELASA</option>
                      <option value="RABU">RABU</option>
                      <option value="KAMIS">KAMIS</option>
                      <option value="JUM'AT">JUM'AT</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Tanggal</label>
                  <div class="col-lg-10">
                    <input type="date" name="tgl" class="form-control" value="" required placeholder="Tanggal">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Nama Pejabat</label>
                  <div class="col-lg-10">
                    <input type="text" name="pejabat" class="form-control" value="" required placeholder="Nama Pejabat">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Jabatan</label>
                  <div class="col-lg-10">
                    <input type="text" name="jabatan" class="form-control" value="" required placeholder="Jabatan">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Jam Mulai</label>
                  <div class="col-lg-10">
                    <input type="time" name="jammulai" class="form-control" value="" required placeholder="Jam Mulai">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Jam Selesai</label>
                  <div class="col-lg-10">
                    <input type="time" name="jamselesai" class="form-control" value="" required placeholder="Jam Selesai">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Kegiatan</label>
                  <div class="col-lg-10">
                    <input type="text" name="kegiatan" class="form-control" value="" required placeholder="Kegiatan">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Dasar Kegiatan</label>
                  <div class="col-lg-10">
                    <input type="text" name="dasarkegiatan" class="form-control" value="" required placeholder="Dasar Kegiatan">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Lokasi</label>
                  <div class="col-lg-10">
                    <input type="text" name="lokasi" class="form-control" value="" required placeholder="Lokasi">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Keterangan</label>
                  <div class="col-lg-10">
                    <input type="text" name="keterangan" class="form-control" value="" required placeholder="Keterangan">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">File</label>
                  <div class="col-lg-10">
                    <input type="file" name="file" class="form-control" value="" required placeholder="File">
                  </div>
                </div>
              </div>
            </div>

            <br>
            <hr>
            <button type="submit" name="btnsimpan" class="btn btn-primary" style="float:right;">Simpan</button>

          </form>
        </div>
        <br>

        <hr>
        <?php
        } ?>

        <div class="table-responsive">
        <table class="table datatable-basic" width="100%">
          <thead>
            <th width="10">No</th>
            <th>Hari</th>
            <th>Tanggal</th>
            <th>Nama Pejabat</th>
            <th>Jabatan</th>
            <th>Jam Mulai</th>
            <th>Jam Selesai</th>
            <th>Kegiatan</th>
            <th>Dasar Kegiatan</th>
            <th>Lokasi</th>
            <th>Keterangan</th>
            <th>File</th>
            <th class="text-center" width="120"></th>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($v_pimpinan->result() as $baris) {
            ?>
              <tr>
                <td><?php echo $no.'.'; ?></td>
                <td><?php echo $baris->hari; ?></td>
                <td><?php echo $baris->tgl; ?></td>
                <td><?php echo $baris->pejabat; ?></td>
                <td><?php echo $baris->jabatan; ?></td>
                <td><?php echo $baris->jammulai; ?></td>
                <td><?php echo $baris->jamselesai; ?></td>
                <td><?php echo $baris->kegiatan; ?></td>
                <td><?php echo $baris->dasarkegiatan; ?></td>
                <td><?php echo $baris->lokasi; ?></td>
                <td><?php echo $baris->keterangan; ?></td>
                <td><?php echo $baris->file; ?></td>
                <td>
                  <a href="web/pimpinan_detail/<?php echo $baris->id_pimpinan; ?>" title="Detail"><span class="icon-eye"></span></a> &nbsp;
                <?php
                if ($this->session->userdata('level@2017') == "admin") {?>
                  <a href="web/pimpinan_edit/<?php echo $baris->id_pimpinan; ?>" title="Edit"><span class="icon-pencil"></span></a> &nbsp;
                  <a href="web/pimpinan_hapus/<?php echo $baris->id_pimpinan; ?>" title="Hapus" onclick="return confirm('Apakah Anda yakin?')"><span class="icon-trash"></span></a>
                <?php
                }?>
                </td>
              </tr>
            <?php
            $no++;
            } ?>
          </tbody>
        </table>
        </div>
      </div>
      <!-- /basic datatable -->
    </div>
    <!-- /dashboard content -->
