<?php
if ($this->session->userdata('level@2019') == "user") {
    redirect('');
} ?>
<!-- Main content -->
<div class="content-wrapper">
  <br><br><br>
  <!-- Content area -->
  <div class="content">

    <!-- Dashboard content -->
    <div class="row">
      <!-- Basic datatable -->
      <div class="panel panel-flat">
        <div class="panel-heading">
          <h5 class="panel-title">Edit Agenda Kegiatan Pimpinan</h5>
          <div class="heading-elements">
            <ul class="icons-list">
              <li><a data-action="collapse"></a></li>
            </ul>
          </div>
        </div>
        <hr>
        <div class="panel-body">
          <?php
          echo $this->session->flashdata('msg');
          ?>
          <form class="form-horizontal" action="" method="post">
            <div class="col-md-12">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-lg-2">Hari</label>
                  <div class="col-lg-3">
                    <select class="form-control" name="hari" required>
                      <option value="">Pilih</option>
                      <option value="SENIN" <?php if ($v_pimpinan->hari == "SENIN"){ echo "selected";} ?>>SENIN</option>
                      <option value="SELASA" <?php if ($v_pimpinan->hari == "SELASA"){ echo "selected";} ?>>SELASA</option>
                      <option value="RABU" <?php if ($v_pimpinan->hari == "RABU"){ echo "selected";} ?>>RABU</option>
                      <option value="KAMIS" <?php if ($v_pimpinan->hari == "KAMIS"){ echo "selected";} ?>>KAMIS</option>
                      <option value="JUM'AT" <?php if ($v_pimpinan->hari == "JUM'AT"){ echo "selected";} ?>>JUM'AT</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Tanggal</label>
                  <div class="col-lg-10">
                    <input type="date" name="tgl" class="form-control" value="<?php echo $v_pimpinan->tgl; ?>" required placeholder="Tanggal">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Nama Pejabat</label>
                  <div class="col-lg-10">
                    <input name="pejabat" rows="8" cols="80" class="form-control" placeholder="Nama Pejabat" value="<?php echo $v_pimpinan->pejabat; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Jabatan</label>
                  <div class="col-lg-10">
                    <input name="jabatan" rows="8" cols="80" class="form-control" placeholder="Jabatan" value="<?php echo $v_pimpinan->jabatan; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Kegiatan</label>
                  <div class="col-lg-10">
                    <input name="kegiatan" rows="8" cols="80" class="form-control" placeholder="Kegiatan" value="<?php echo $v_pimpinan->kegiatan; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Dasar Kegiatan</label>
                  <div class="col-lg-10">
                    <input name="dasarkegiatan" rows="8" cols="80" class="form-control" placeholder="Dasar Kegiatan" value="<?php echo $v_pimpinan->dasarkegiatan; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Jam Mulai</label>
                  <div class="col-lg-10">
                    <input type="time" name="jammulai" class="form-control" value="<?php echo $v_pimpinan->jammulai; ?>" required placeholder="Jam Mulai">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Jam Selesai</label>
                  <div class="col-lg-10">
                    <input type="time" name="jamselesai" class="form-control" value="<?php echo $v_pimpinan->jamselesai; ?>" required placeholder="Jam Selesai">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Lokasi</label>
                  <div class="col-lg-10">
                    <input name="lokasi" rows="8" cols="80" class="form-control" placeholder="Lokasi" value="<?php echo $v_pimpinan->lokasi; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Keterangan</label>
                  <div class="col-lg-10">
                    <input type="radio" name="keterangan" value="Ada">Ada
                    <input type="radio" name="keterangan" value="Tidak Ada">Tidak Ada
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">File</label>
                  <div class="col-lg-10">
                    <input type="file" name="file" rows="8" cols="80" class="form-control" placeholder="File" value="<?php echo $v_pimpinan->file; ?>">
                  </div>
                </div>
              </div>
            </div>

            <br>
            <hr>
            <a href="web/pimpinan" class="btn btn-default">Kembali</a>

            <button type="submit" name="btnsimpan" class="btn btn-primary" style="float:right;">Simpan</button>

          </form>
        </div>
        <br>

      </div>
      <!-- /basic datatable -->
    </div>
    <!-- /dashboard content -->
