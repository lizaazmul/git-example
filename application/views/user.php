<?php
if ($this->session->userdata('level@2017') == "user") {
    redirect('');
}?>
<!-- Main content -->
<div class="content-wrapper">
  <br><br><br>
  <!-- Content area -->
  <div class="content">

    <!-- Dashboard content -->
    <div class="row">
      <!-- Basic datatable -->
      <div class="panel panel-flat">
        <div class="panel-heading">
          <h5 class="panel-title">
            Tambah Admin</h5>
          <div class="heading-elements">
            <ul class="icons-list">
              <li><a data-action="collapse"></a></li>
            </ul>
          </div>
        </div>
        <hr>
        <?php
        if ($this->session->userdata('level@2017') == "admin") {?>
        <div class="panel-body">
          <?php
          echo $this->session->flashdata('msg');
          ?>
          <form class="form-horizontal" action="" method="post">
            <div class="col-md-12">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label col-lg-2">Username</label>
                  <div class="col-lg-10">
                    <input type="text" name="username" class="form-control" value="" required placeholder="Username">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-lg-2">Password</label>
                  <div class="col-lg-10">
                    <input type="password" name="password" class="form-control" value="" required placeholder="Password">
                  </div>
                </div>
              </div>
            </div>

            <br>
            <hr>
            <button type="submit" name="btnsimpan" class="btn btn-primary" style="float:right;">Simpan</button>

          </form>
        </div>
        <br>

        <hr>
        <?php
        } ?>
        <div class="table-responsive">
        <table class="table datatable-basic" width="100%">
          <thead>
            <th width="10">No</th>
            <th>USERNAME</th>
            <th class="text-center" width="120"></th>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($v_user->result() as $baris) {
            ?>
              <tr>
                <td><?php echo $no.'.'; ?></td>
                <td><?php echo $baris->username; ?></td>
                <td>
                  <a href="web/user_hapus/<?php echo $baris->id_user; ?>" title="Hapus" onclick="return confirm('Anda Yakin?')"><span class="icon-trash"></span></a> &nbsp;
                </td>
              </tr>
            <?php
            $no++;
            } ?>
          </tbody>
        </table>
        </div>
      </div>
      <!-- /basic datatable -->
    </div>
    <!-- /dashboard content -->
